﻿namespace API.Models
{
    using Newtonsoft.Json;

    public class Country
    {
        [JsonProperty("numericCode")]
        public string Code { get; set; }

        [JsonProperty("capital")]
        public string Capital { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("subregion")]
        public string Subregion { get; set; }
    }
}