﻿namespace API
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Net.Http;
    using API.Models;
    using Newtonsoft.Json;
    using Xamarin.Essentials;
    using Xamarin.Forms;

    public partial class MainPage : ContentPage
    {
        const string urlBase = "https://restcountries.eu/rest/v2/";

        public MainPage()
        {
            InitializeComponent();
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                await App.Current.MainPage.DisplayAlert("API", "No Internet", "OK");
                return;
            }

            try
            {
                indicator.IsRunning = true;

                HttpClient client = new HttpClient();

                string content = await client.GetStringAsync(urlBase);

                List<Country> data = JsonConvert.DeserializeObject<List<Country>>(content);

                var countries = new ObservableCollection<Country>(data);
                CollectionView.ItemsSource = countries;
            }
            catch(Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("API", ex.Message, "OK");
            }
            finally
            {
                indicator.IsRunning = false;
            }
        }
    }
}